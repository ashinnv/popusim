module gitlab.com/ashinnv/popusim

go 1.19

require gitlab.com/ashinnv/person v1.1.1

replace gitlab.com/ashinnv/person => ../person
