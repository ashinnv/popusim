package main

import (
	"gitlab.com/ashinnv/person"
)

/*
	Hold list of people who are politically different. Set them in a 2d grid and
	"walk them around". When close enough, they "meet", "argue", or "befriend". Then, we
	use an injection of ads that change people's interactions

*/

func main() {

	personMap := make(map[string]Person)
	person.Genpop(personMap)

	stopKill()

}

// Stop the program from stopping.
func stopKill() {
	killchan := make(chan bool)
	_ = <-killchan
	return
}

func Meet(people ...Person) error {

}

func Argue(people ...Person) error {

}

func Befriend(person Person, friend Person) error {

}
