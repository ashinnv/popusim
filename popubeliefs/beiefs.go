package popubeliefs

// Beliefs, positive number is more conservative. Negative is more liberal position.
type Belief struct {
	TransRights    float32
	GayMarriage    float32
	Federalisim    float32
	BodyPositivity float32
	ForeignAide    float32
	GunRights      float32
}
