module gitlab.com/ashinnv/person

go 1.18

require gitlab.com/ashinnv/popubeliefs v1.1.1

replace gitlab.com/ashinnv/popubeliefs => ../popubeliefs/
