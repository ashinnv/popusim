package person

import (
	bef "gitlab.com/ashinnv/popubeliefs"
)

type Pos struct {
	Xpos int
	Ypos int
}

type Person struct {
	Name string

	//Location
	CurrentPos Pos
	HomePos    Pos

	Beliefs bef.Belief

	//Influentialism
	Sensitivity float32 //How easily influenced
	InfluStreng float32 //How easily will influence
	MotilitySub int     //How many spaces moves minimum per tick
	MotilityMax int     //How many spaces moves max per tick
	ContactsMin int     //Minimum number of people talked to per round
	ContactsMax int     //See above
}

func Genpop() map[string]Person {

}
